Changelog
=========

2.0.2
-----
 - updated buildout configuration

2.0.1
-----
 - updated buildout configuration

2.0.0
-----
 - migrated to Pyramid 2.0
 - added support for Python 3.10 and 3.11

1.1.0
-----
 - removed support for Python < 3.7

1.0.3
-----
 - removed Travis-CI configuration

1.0.2
-----
 - added string constant in subset module
 - updated doctests syntax

1.0.1
-----
 - updated doctests using ZCA hook

1.0.0
-----
 - initial release
